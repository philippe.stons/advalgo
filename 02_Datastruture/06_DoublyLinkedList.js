"use strict";

/*
    Node :
        - next => next Node
        - data

    List 
    Node1 <=> Node2 <=> Node3 <=> Node4
     ^ head                       ^ tail

    ----------
    | NODE 4 |
      ......
    | NODE 1 |
      ......
    | NODE 2 |
      ......
    | NODE 3 |
*/

class ListNode
{
    constructor(data, next)
    {
        this.data = data;
        this.next = next;
        this.prev = null;
    }
}

class LinkedList
{
    constructor()
    {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    pushFront(data)
    {
        let newNode = new ListNode(data, this.head);

        if(this.tail === null)
        {
            this.tail = newNode;
        }

        if(this.head)
        {
            this.head.prev = newNode;
        }
        
        this.head = newNode;
        
        this.length++;

        return this.head;
    }

    pushBack(data)
    {
        let newNode = new ListNode(data, null);
        
        // O(1)
        if(this.tail !== null)
        {
            this.tail.next = newNode;
        }

        newNode.prev = this.tail;
        this.tail = newNode;

        if(this.head === null)
        {
            this.head = newNode;
        }

        this.length++;

        return this.tail;
    }

    insertAt(data, index)
    {
        if(!Number.isInteger(index) || index < 0 || index > this.length)
        {
            console.log("Erreur index invalide");
            return null;
        }
        
        let newNode = null;

        if(index === 0)
        {
            return this.pushFront(data);
        } else  if(index === this.length)
        {
            return this.pushBack(data);
        } else 
        {
            let previousNode = this.head;

            for(let i = 0; i < index - 1; i++)
            {
                previousNode = previousNode.next;
            }

            let nextNode = previousNode.next;
            newNode = new ListNode(data, nextNode);
            previousNode.next = newNode;
            newNode.prev = previousNode;
            nextNode.prev = newNode;
        }

        this.length++;

        return newNode;
    }

    popFront()
    {
        let elementData = this.head.data;
        
        if(this.head == this.tail)
        {
            this.tail = null;
        }

        this.head = this.head.next;
        this.head.prev = null;

        this.length--;

        return elementData;
    }

    popBack()
    {
        let elemData = this.tail.data;

        if(this.head == this.tail)
        {
            this.head = null;
        }

        this.tail = this.tail.prev;
        this.tail.next = null;
        this.length--;

        return elemData;
    }

    removeIndex(index)
    {
        if(!Number.isInteger(index) || index < 0 || index > this.length)
        {
            console.log("Erreur index invalide");
            return null;
        }

        if(index === 0)
        {
            return this.popFront();
        } else if(index === this.length - 1)
        {
            return this.popBack();
        }

        let previousNode = this.head;

        for(let i = 0; i < index - 1; i++)
        {
            previousNode = previousNode.next;
        }
        // previousNode -> deletedNode -> nextNode
        let deletedNode = previousNode.next;
        let nextNode = deletedNode.next;
        nextNode.prev = previousNode;
        // previousNode -> nextNode 
        previousNode.next = nextNode;

        this.length--;

        return deletedNode.data;
    }

    printList()
    {
        let array = [];
        let currentNode = this.head;
        while(currentNode !== null)
        {
            array.push(currentNode.data);
            currentNode = currentNode.next;
        }

        console.log(array.join(' <=> '));
        this.printReverseList();
    }

    printReverseList()
    {
        let array = [];
        let currentNode = this.tail;
        while(currentNode !== null)
        {
            array.push(currentNode.data);
            currentNode = currentNode.prev;
        }

        console.log(array.join(' <=> '));
    }
}

let list = new LinkedList();
list.pushFront(5);
list.printList();
list.pushFront(12);
list.printList();
list.pushBack(32);
list.printList();
list.insertAt(45, 1);
list.printList();
console.log(list.popFront());
list.printList();
console.log(list.popBack());
list.printList();
list.insertAt(42, 1);
list.printList();
console.log(list.removeIndex(1));
list.printList();