/*
    LIFO (Last in first out)
    
    -------------
    | ELEMENT 1 |   [0]
    | ELEMENT 2 |   [1]
    | ELEMENT 3 |   [2]
    | ELEMENT 4 |   [...]
    | ELEMENT 5 |
    | ELEMENT 6 |
    -------------

    push(ELEMENT 7)
    -------------
    | ELEMENT 1 |   [0]
    | ELEMENT 2 |   [1]
    | ELEMENT 3 |   [2]
    | ELEMENT 4 |   [...]
    | ELEMENT 5 |
    | ELEMENT 6 |
    | ELEMENT 7 |
    -------------
    
    pop() => ELEMENT 1
    -------------
    | ELEMENT 2 |   [0]
    | ELEMENT 3 |   [1]
    | ELEMENT 4 |   [...]
    | ELEMENT 5 |
    | ELEMENT 6 |
    | ELEMENT 7 |
    -------------
*/