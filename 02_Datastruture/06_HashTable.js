"use strict";

/*
    hashMap[key] => value

    --------------
    |   Bucket   | -> Liste node1 => node2 => node3 ...
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   |
    |   Bucket   | -> liste.pushback(data)
    |   Bucket   |
    |   Bucket   |
    --------------

    key % bucketSize;
*/

class ListNode
{
    constructor(key, data, next)
    {
        this.key = key;
        this.data = data;
        this.next = next;
        this.prev = null
    }
}

class LinkedList
{
    constructor()
    {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    pushFront(key, data)
    {
        let newNode = new ListNode(key, data, this.head);

        if(this.tail === null)
        {
            this.tail = newNode;
        }

        if(this.head)
        {
            this.head.prev = newNode;
        }
        
        this.head = newNode;
        
        this.length++;

        return this.head;
    }

    pushBack(key, data)
    {
        let newNode = new ListNode(key, data, null);
        
        // O(1)
        if(this.tail !== null)
        {
            this.tail.next = newNode;
        }

        newNode.prev = this.tail;
        this.tail = newNode;

        if(this.head === null)
        {
            this.head = newNode;
        }

        this.length++;

        return this.tail;
    }

    insertAt(key, data, index)
    {
        if(!Number.isInteger(index) || index < 0 || index > this.length)
        {
            console.log("Erreur index invalide");
            return null;
        }
        
        let newNode = null;

        if(index === 0)
        {
            return this.pushFront(data);
        } else  if(index === this.length)
        {
            return this.pushBack(data);
        } else 
        {
            let previousNode = this.head;

            for(let i = 0; i < index - 1; i++)
            {
                previousNode = previousNode.next;
            }

            let nextNode = previousNode.next;
            newNode = new ListNode(key, data, nextNode);
            previousNode.next = newNode;
            newNode.prev = previousNode;
            nextNode.prev = newNode;
        }

        this.length++;

        return newNode;
    }

    popFront()
    {
        let elementData = this.head.data;
        
        if(this.head == this.tail)
        {
            this.tail = null;
        }

        this.head = this.head.next;
        this.head.prev = null;

        this.length--;

        return elementData;
    }

    popBack()
    {
        let elemData = this.tail.data;

        if(this.head == this.tail)
        {
            this.head = null;
        }

        this.tail = this.tail.prev;
        this.tail.next = null;
        this.length--;

        return elemData;
    }

    removeIndex(index)
    {
        if(!Number.isInteger(index) || index < 0 || index > this.length)
        {
            console.log("Erreur index invalide");
            return null;
        }

        if(index === 0)
        {
            return this.popFront();
        } else if(index === this.length - 1)
        {
            return this.popBack();
        }

        let previousNode = this.head;

        for(let i = 0; i < index - 1; i++)
        {
            previousNode = previousNode.next;
        }
        // previousNode -> deletedNode -> nextNode
        let deletedNode = previousNode.next;
        let nextNode = deletedNode.next;
        nextNode.prev = previousNode;
        // previousNode -> nextNode 
        previousNode.next = nextNode;

        this.length--;

        return deletedNode.data;
    }

    remove(key)
    {
        let currentNode = this.searchKey(key);

        if(currentNode !== null)
        {
            let previousNode = currentNode.prev;
            let nextNode = currentNode.next;

            if(previousNode !== null)
            {
                previousNode.next = nextNode;
            }

            if(nextNode !== null)
            {
                nextNode.prev = previousNode;
            }

            if(currentNode === this.head)
            {
                this.head = null;
            }

            if(currentNode === this.tail)
            {
                this.tail = null;
            }   

            this.length--;

            return currentNode.data;
        }

        return null;
    }

    searchKey(key)
    {
        let currentNode = this.head;

        for(let i = 0; i < this.length; i++)
        {
            if(currentNode.key === key)
            {
                return currentNode;
            }

            currentNode = currentNode.next;
        }

        return null;
    }

    printList()
    {
        let array = [];
        let currentNode = this.head;
        while(currentNode !== null)
        {
            array.push(currentNode.data);
            currentNode = currentNode.next;
        }

        console.log(array.join(' <=> '));
        // this.printReverseList();
    }

    printReverseList()
    {
        let array = [];
        let currentNode = this.tail;
        while(currentNode !== null)
        {
            array.push(currentNode.data);
            currentNode = currentNode.prev;
        }

        console.log(array.join(' <=> '));
    }
}

class HashTable
{
    constructor(size = 20)
    {
        this.buckets = [];
        this.size = size;
    }

    simpleHash(key)
    {
        return key.toString().length;
    }

    hashKey(key)
    {
        if(!Number.isInteger(key))
        {
            key = this.simpleHash(key);
        }

        return key % this.size;
    }

    addOrUpdate(key, data) // Best O(1) worst O(n)
    {
        const hashedKey = this.hashKey(key);

        if(!this.buckets[hashedKey])
        {
            this.buckets[hashedKey] = new LinkedList();
        } else 
        {
            let element = this.buckets[hashedKey].searchKey(key);

            if(element !== null)
            {
                element.data = data;
                return data;
            }
        }

        this.buckets[hashedKey].pushBack(key, data);

        return data;
    }

    remove(key)
    {
        const hashedKey = this.hashKey(key);

        if(this.buckets[hashedKey])
        {
            this.buckets[hashedKey].remove(key);
        }
    }

    get(key)
    {
        const hashedKey = this.hashKey(key);

        if(this.buckets[hashedKey])
        {
            return this.buckets[hashedKey].searchKey(key);
        }

        return null;
    }

    printHashTable()
    {
        for(const key in this.buckets)
        {
            console.log(`Key : ${key}`);
            this.buckets[key].printList();
        }
    }
}

let hTable = new HashTable();

hTable.addOrUpdate(5, "test");
hTable.addOrUpdate("Vive le formage!", "C'est bon le roblochon!");
hTable.addOrUpdate('123456', "Mon super mot de passe est la clé");
hTable.printHashTable();
hTable.addOrUpdate('12345', "Mon super mot de passe est la clé");
hTable.printHashTable();
console.log(hTable.get('12345'));
console.log(hTable.remove('12345'));
hTable.printHashTable();
console.log(hTable.get(5));