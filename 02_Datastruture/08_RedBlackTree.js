/*
    Un des problème qu'on rencontre avec les binary tree 
    c'est qu'ils peuvent se comporter comme une liste.

    1
     \
      5
       \
        6
         \
          7
           \
            8
            ...

    Le RedBlack tree tend à résoudre ce problème.

    En propsant un set de règles à respecter pour équilibré l'arbre :
        1 Les nodes sont soit noir soit rouge
        2 La root et les leaves (NIL) sont obligatoirement noir
        3 Si une node est rouge alors ses enfants DOIVENT être noir
        4 Tous les chemins menant d'une node à ses descendant NILs contiennent le même nombre
          de node noir.

    Pour réaranger le tree, on va utiliser deux méthode principalement :
        - la rotation O(1)
        - et le swap de couleur O(1)

    function rotateLeft(T, x)
        y = x.right         // On récupère l'enfant de la node à rotate.
        x.right = y.left    // le nouvel enfant de droite devient l'ex enfant de gauche
        if y.left != NIL    // si l'enfant de gauche existe
            y.left.parent = x   // on lui assigne son nouveau parent.
        y.parent = x.parent     // on assigne le nouveau parent de y
        if x.parent == NIL
            T.root = y
        else 
            if x == x.parent.left
                x.parent.left = y
            else
                x.parent.right = y
        y.left = x
        x.parent = y

    Pour l'insertion dans un tree on va respecter certaines règles :
        - On insert toujours une nouvelle node ROUGE
        - On va recolorer et faire des rotations pour réorganiser le tree.

    Une fois la node insérée on a 4 scénarios possibles :
        1 La node insrée est la root
            -> on recolorie la node en noir.
        2 L'oncle de la node insérée est rouge
            -> On recolorie le parent, l'oncle et le grand parent.
        3 Loncle de la node insrée est noir et la node insérée est dans le même sense quand l'oncle.
            -> On fait une rotation de A dans la direction opposée de Z (ce qui trigger le cas 4)
        4 L'oncle de la node est noir et la node est dans le sense inverse de l'oncle par rapport à son parent.
            -> On fait une rotation du parent et du grand parent. et on les recolorie.

    function insert(T, z)
        y = NIL
        x = T.root

        while x != NIL
            y = x
            if z.key < x.key
                x = x.left
            else
                x = x.right
        
        z.parent = y

        if y == NIL
            T.root = z
        else if z.key < y.key
            y.left = z
        else 
            y.right = z

        z.left = NIL
        z.right = NIL
        z.color = RED

    insertFixup(T, z);

    function insertFixup(T, z):
        while z.parent.color == RED
            if z.parent == z.parent.parent.left
                y = z.parent.parent.right   // oncle
                if y.color == RED           // Cas oncle rouge
                    z.parent.color = BLACK
                    y.color = BLACK
                    z.parent.parent.color = RED
                    z = z.parent.parent
                else
                    if z == z.parent.right  // Cas enfant inséré du même coté que l'oncle
                        z = z.parent
                        leftRotate(T, z)
                    z.parent.color = BLACK
                    z.parent.parent.color = RED
                    rightRotate(T, z.parent.parent) 
            else
                y = z.parent.parent.left   // oncle
                if y.color == RED           // Cas oncle rouge
                    z.parent.color = BLACK
                    y.color = BLACK
                    z.parent.parent.color = RED
                    z = z.parent.parent
                else
                    if z == z.parent.left  // Cas enfant inséré du même coté que l'oncle
                        z = z.parent
                        rightRotate(T, z)
                    z.parent.color = BLACK  // Cas enfant du coté opposé à 
                                            // l'oncle par rapport à son parent.
                    z.parent.parent.color = RED
                    leftRotate(T, z.parent.parent)

        root.color = BLACK  // Cas on insert sur la root.

    Delete :
        1 La node qu'on delete est une leaf rouge
            -> delete la node.
        2 La node double black (DB) est le root :
            -> la DB devient noir.
        3 Le frère de la DB est noir et ses enfants sont noir :
            - on supprime DB
            - Le frère devient rouge
            - Si le parent est noir, alors il devient le DB, si non il devient noir.
        4 Si le frère est rouge :
            - On échange les couleurs du parent et du frère.
            - On fait une rotation sur la parent dans la direction DB
            - On continue à regarder si d'autres cas s'applique.
        5 Le frère est noir et que son enfant proche de DB est rouge
            - On échange les couleurs du frère et l'enfant rouge
            - On fait une rotation sur le frère dans la direction opposé à DB
            - On applique le cas 6
        6 Le frère est noir et que son enfant éloigner de DB est rouge
            - échange les couleurs du parent de DB avec son frère.
            - On fait une rotation du parent en direction DB
            - On retire Le DB devient une node noir
            - On change la couleur de l'enfant éloigné.

    function delete(T, z)
        if z.left == NIL || z.right = NIL
            y = z
        else
            y = findMin(z.right)

        if y.left != NIL
            x = y.left
        else
            x = y.right

        x.parent = y.parent

        if y.parent == NIL
            T.root = x  // CAS 2
        else if y == y.parent.left
            y.parent.left = x
        else
            y.parent.right = x
        
        if y != z
            z.key = y.key
            z.value = y.value

        if y.color == BLACK
            deleteFixup(T, x)

    function deleteFixup(T, x)
        while x != T.root && x.color == BLACK
            if x == x.parent.left
                w = x.parent.right
                if w.color == RED   // Cas 4 frère rouge
                    w.color = black
                    x.parent.color = RED
                    leftRotation(T, x.parent)
                    w = x.parent.right
                if w.left.color == BLACK and w.right.color == BLACK
                    w.color = RED
                    x = x.parent
                else
                    if w.right.color == BLACK   // Cas 5 frère noir, enfant proche rouge
                        w.left.color = BLACK
                        w.color = RED
                        rightRotate(T, w)
                        w = x.parent.right
                    w.color = x.parent.color    // cas 6 frère noir, enfant éloigner rouge
                    x.parent.color = BLACK
                    w.right.color = BLACK
                    leftRotation(T, x.parent)
                    x = T.root
            else (left => right)
                if w.color == RED   // Cas 4 frère rouge
                    w.color = black
                    x.parent.color = RED
                    rightRotation(T, x.parent)
                    w = x.parent.left
                if w.right.color == BLACK and w.left.color == BLACK
                    w.color = RED
                    x = x.parent
                else
                    if w.left.color == BLACK   // Cas 5 frère noir, enfant proche rouge
                        w.right.color = BLACK
                        w.color = RED
                        leftRotate(T, w)
                        w = x.parent.left
                    w.color = x.parent.color    // cas 6 frère noir, enfant éloigner rouge
                    x.parent.color = BLACK
                    w.left.color = BLACK
                    rightRotation(T, x.parent)
                    x = T.root
        endwhile
        x.color = BLACK     // Cas 2 part 2 
*/

const RED_NODE = 1;
const BLACK_NODE = 0;

class RBTreeNode
{
    constructor(parent = null)
    {
        this.key = null;
        this.value = null;
        this.left = null;
        this.right = null;
        this.color = BLACK_NODE;
        this.parent = parent;
    }
}

class RedBlackTree
{
    constructor()
    {
        this.root = null;
        this.size = 0;
        this.min = null;
    }

    isNilNode(node)
    {
        return node === null;
    }

    createNewNode(key, value)
    {
        let node = new RBTreeNode(null);

        node.key = key;
        node.value = value;

        node.color = RED_NODE;
        
        return node;
    }

    rotateLeft(node)
    {
        const y = node.right;

        node.right = y.left;

        if(!this.isNilNode(y.left))
        {
            y.left.parent = node;
        }

        y.parent = node.parent;

        if(this.isNilNode(node.parent))
        {
            this.root = y;
        } else 
        {
            if(node === node.parent.left)
            {
                node.parent.left = y;
            } else 
            {
                node.parent.right = y;
            }
        }

        y.left = node;

        node.parent = y;
    }

    rotateRight(node)
    {
        const y = node.left;

        node.left = y.right;

        if(!this.isNilNode(y.right))
        {
            y.right.parent = node;
        }

        y.parent = node.parent;

        if(this.isNilNode(node.parent))
        {
            this.root = y;
        } else 
        {
            if(node === node.parent.right)
            {
                node.parent.right = y;
            } else 
            {
                node.parent.left = y;
            }
        }

        y.right = node;

        node.parent = y;
    }

    findParent(fromNode, node)
    {
        let y = null;
        let x = fromNode;

        while(!this.isNilNode(x))
        {
            y = x;
            
            if(node.key < x.key)
            {
                x = x.left;
            } else 
            {
                x = x.right;
            }
        }

        return y;
    }

    insert(key, value)
    {
        let y = null;
        let x = this.root;

        const z = this.createNewNode(key, value);

        if(this.root == null)
        {
            this.root = z;
            this.min = z;
            z.color = BLACK_NODE;
            z.parent = null;
        } else 
        {
            y = this.findParent(x, z);

            z.parent = y;

            if(z.key < y.key)
            {
                y.left = z;
            } else 
            {
                y.right = z;
            }

            z.color = RED_NODE;
            this.insertFixup(z);
        }

        if(this.min.key > z.key)
        {
            this.min = z;
        }
    }

    insertFixup(node)
    {
        while(node.parent != null &&  node.parent.color == RED_NODE)
        {
            let uncle = null;

            if(node.parent == node.parent.parent.left)
            {
                uncle = node.parent.parent.right;

                if(uncle != null && uncle.color === RED_NODE)
                {
                    node.parent.color = BLACK_NODE;
                    uncle.color = BLACK_NODE;
                    node.parent.parent.color = RED_NODE;
                    node = node.parent.parent;
                } else 
                {
                    if(node == node.parent.right)
                    {
                        node = node.parent;
                        this.rotateLeft(node);
                    }

                    node.parent.color = BLACK_NODE;
                    node.parent.parent.color = RED_NODE;
                    this.rotateRight(node.parent.parent);
                }
            } else 
            {
                uncle = node.parent.parent.left;

                if(uncle != null && uncle.color === RED_NODE)
                {
                    node.parent.color = BLACK_NODE;
                    uncle.color = BLACK_NODE;
                    node.parent.parent.color = RED_NODE;
                } else 
                {
                    if(node === node.parent.left)
                    {
                        node = node.parent;
                        this.rotateRight(node);
                    }

                    node.parent.color = BLACK_NODE;
                    node.parent.parent.color = RED_NODE;
                    this.rotateLeft(node.parent.parent);
                }
            }
        }

        this.root.color = BLACK_NODE;
    }

    search(node, key)
    {
        while(node !== null)
        {
            if(key < node.key)
            {
                node = node.left
            } else if(key > node.key)
            {
                node = node.right;
            } else
            {
                return node;
            }
        }

        return null;
    }

    transplant(u, v)
    {
        if(u.parent == null)
        {
            this.root = v;
        } else if(u === u.parent.left)
        {
            u.parent.left = v;
        } else
        {
            u.parent.right = v;
        }

        if(v && u)
        {
            v.parent = u.parent;
        }
    }

    findMin(node)
    {
        let x = node;
    
        while(x.left !== null)
        {
            x = x.left;
        }

        return x;
    }

    remove(key)
    {
        let z = this.search(this.root, key);
        let y = null;
        let x = null;

        if(z == null)
        {
            return;
        }

        if(z.left === null || z.right === null)
        {
            y = z;
        } else 
        {
            y = this.findMin(z.right);
        }

        if(y.left !== null)
        {
            x = y.left;
        } else 
        {
            x = y.right
        }

        if(x)
        {
            x.parent = y.parent;
        }

        if(y.parent === null)
        {
            this.root = x;
        } else if(y.parent.left === y)
        {
            y.parent.left = x;
        } else 
        {
            y.parent.right = x;
        }

        if(y !== z)
        {
            z.key = y.key;
            z.value = y.value;
        }
        if(y.color === BLACK_NODE)
        {
            this.removeFixup(x);
        }
    }

    removeFixup(x)
    {
        if(x === null)
        {
            return;
        }

        while(x != this.root && x.color == BLACK_NODE)
        {
            if(x === x.parent.left)
            {
                let w = x.parent.right;
                if(w.color == RED_NODE)
                {
                    w.color = BLACK_NODE
                    x.parent.color = RED_NODE;
                    this.rotateLeft(x.parent);
                    w = x.parent.right;
                }

                if(w.left.color === BLACK_NODE && w.right.color === BLACK_NODE)
                {
                    w.color = RED_NODE;
                    x = x.parent;
                } else 
                {
                    if(w.left.color === RED_NODE)
                    {
                        w.left.color = BLACK_NODE;
                        w.color = RED_NODE;
                        this.rotateRight(w);
                        w = x.parent.right;
                    }
                    w.color = x.parent.color;
                    x.parent.color = BLACK_NODE;
                    w.right.color = BLACK_NODE;
                    this.rotateLeft(x.parent);
                    x = this.root;
                }
            } else
            {
                let w = x.parent.left;
                if(w.color == RED_NODE)
                {
                    w.color = BLACK_NODE
                    x.parent.color = RED_NODE;
                    this.rotateRight(x.parent);
                    w = x.parent.left;
                }

                if(w.right.color === BLACK_NODE && w.left.color === BLACK_NODE)
                {
                    w.color = RED_NODE;
                    x = x.parent;
                } else 
                {
                    if(w.right.color === RED_NODE)
                    {
                        w.right.color = BLACK_NODE;
                        w.color = RED_NODE;
                        this.rotateLeft(w);
                        w = x.parent.left;
                    }
                    w.color = x.parent.color;
                    x.parent.color = BLACK_NODE;
                    w.left.color = BLACK_NODE;
                    this.rotateRight(x.parent);
                    x = this.root;
                }
            }
        }

        x.color = BLACK_NODE;
    }

    printTree(node)
    {
        if(node !== null && node === this.root)
        {
            console.log(`Root : ${node.key} - ${node.value}`);
        }

        if(!this.isNilNode(node))
        {
            this.printTree(node.left);
            console.log(node.value, node.color);
            this.printTree(node.right);
        }
    }
}

let btree = new RedBlackTree();
btree.insert(15, "15");
btree.insert(25, "25");
btree.insert(10, "10");
btree.insert( 7, "7");
btree.insert(22, "22");
btree.insert(31, "31");
btree.insert(13, "13");
btree.insert(27, "27");

btree.printTree(btree.root);
btree.remove(7);
btree.printTree(btree.root);
while(btree.root)
{
    console.log(btree.root.key);
    btree.remove(btree.root.key);
    btree.printTree(btree.root);
}