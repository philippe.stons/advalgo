/*
    Binary Search trees

    Imaginons un array qui est dans l'ordre. Disons que je veuille savoir si la valeur 100 se trouve dedans.

    1 | 2 | 12 | 23 | 32 | 42 | 54 | 55 | 100 | 134

    La manière la plus simple de savoir si l'array contient bien 100 serait de parcourir celui ci et d'en comparer chaque valeur.
*/

let arr = [1, 2, 12, 23, 32, 42, 54, 55, 100, 134]

for(let i = 0; i < arr.length; i++)
{
    if(arr[i] === 100)
    {
        console.log("FOUND");
        break;
    }
}

/*
    Cette méthode fonctionne bien sur les petits tableau, mais sera particulièrement gourmande pour les tableaux de grande tailles,
    car sa complexité est linéaire (soit O(N) ).

    Serait-il possible de faire en sorte d'avoir une algorithme de recherche plus rapide?

    Voyons la méthode du binary search.

    Pour cette méthode, nous allons diviser le tableau en deux parties égales et ensuite nous allons vérifier si la valeur cherchée 
    est plus grande ou plus petite que la valeur centrale. Si celle-ci est plus grande, alors nous faisons la même chose sur la partie 
    supérieur du tableau, si non sur la partie inférieur.
*/

// 1282
arr = [1, 2, 3, 12, 23, 32, 42, 54, 55, 100, 134];
let it = 0;
console.log(binarySearch(arr, 0, arr.length - 1, 100));
console.log(it);

function binarySearch(arr, start, end, value)
{
    if(start < 0 || start > end || end >= arr.length)
    {
        return { found: false, error : "start or end index invalid" };
    }

    let mid = Math.floor((start + end) / 2);
    it++;

    if(arr[mid] === value)
    {
        return { found: true, index: mid };
    }

    if(arr[mid] > value)
    {
        return binarySearch(arr, start, mid - 1, value);
    } else 
    {
        return binarySearch(arr, mid + 1, end, value);
    }
}

class BTNode 
{
    constructor(key, data)
    {
        this.key = key;
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

class BinarySearchTree
{
    constructor()
    {
        this.root = null;
    }

    insert(key, data)
    {
        var newNode = new BTNode(key, data);

        if(this.root === null)
        {
            this.root = newNode;
        } else 
        {
            this.insertInNode(this.root, newNode);
        }
    }

    insertInNode(node, newNode)
    {
        if(newNode.key < node.key)
        {
            if(node.left === null)
            {
                node.left = newNode;
            } else
            {
                this.insertInNode(node.left, newNode);
            }
        } else 
        {
            if(node.right === null)
            {
                node.right = newNode;
            } else 
            {
                this.insertInNode(node, newNode);
            }
        }
    }

    remove(key)
    {
        return this.removeFromNode(this.root, null, key);
    }

    removeFromNode(node, key)
    {
        if(node === null)
        {
            return null;
        }

        if(key < node.key)
        {
            node = this.removeFromNode(node.left, key);
            return node;
        }

        if(key > node.key)
        {
            node = this.removeFromNode(node.right, key);
            return node;
        } else
        {
            if(node.left === null && node.right === null)
            {
                return null;
            }

            if(node.left === null)
            {
                node = node.right;
                return node;
            } else if(node.right === null)
            {
                node = node.left
                return node;
            }

            let aux = this.findMin(node.right);
            node.data = aux.data;
            node.key = aux.key;

            node.right = this.removeFromNode(node.right, aux.key);
            return node;
        }
    }

    findMin(node)
    {
        if(node.left === null)
        {
            return node;
        }

        return this.findMin(node.left);
    }

    search(key)
    {
        return this.searchFromNode(key);
    }

    searchFromNode(node, key)
    {
        if(node === null)
        {
            return null;
        }

        if(key < node.key)
        {
            return this.searchFromNode(node.left, key);
        } else if(key > node.key)
        {
            return this.searchFromNode(node.right, key);
        }

        return node;
    }
}