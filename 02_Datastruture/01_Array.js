/*
    Un Array est une structure qui contient une suite d'élément contigu en mémoire.
  
    -------------
    | ELEMENT 1 |   [0]
    | ELEMENT 2 |   [1]
    | ELEMENT 3 |   [2]
    | ELEMENT 4 |   [...]
    | ELEMENT 5 |
    | ELEMENT 6 |
    -------------
      VARIABLE 

      ....

    | ELEMENT 1 |   [0]
    | ELEMENT 2 |   [1]
    | ELEMENT 3 |   [2]
    | ELEMENT 4 |   [...]
    | ELEMENT 5 |
    | ELEMENT 6 |
    | ELEMENT 7 |
*/