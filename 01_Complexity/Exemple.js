function example1(a, b) // O(n + b)
{
    for(let i = 0; i < a; i++)
    {

    }

    for(let i = 0; i < b; i++)
    {

    }
}

function example1(a, b) // O(a * b)
{
    for(let i = 0; i < a; i++)
    {
        for(let j = 0; j < b; j++)
        {
    
        }
    }
}