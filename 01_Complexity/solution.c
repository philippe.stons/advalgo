#include <stdio.h>
#include <stdlib.h>

void solution(int n)    // O(N^3)
{
    for(int a = 0; a <= n; a++)
    {
        for(int b = 0; b <= n; b++)
        {   
            for(int c = 0; c <= n; c++)
            {
                if(a + b + c == n)
                {
                    printf("a : %d b : %d c : %d\n", a, b, c);
                }
            }
        }
    }
}


void solution2(int n) // O(N^2)
{
    for(int a = 0; a <= n; a++)
    {
        for(int b = 0; b <= n; b++)
        {   
            int c = n - (a + b);

            if(c >= 0)
            {
                printf("a : %d b : %d c : %d\n", a, b, c);
            }
        }
    }
}

int main(int argc, char ** argv)
{
    solution2(500);

    return 0;
}