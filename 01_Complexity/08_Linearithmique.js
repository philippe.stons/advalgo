/*
    O(n log n)
*/

function nLogN(n)
{
    let i = n;
    let nArr = 0;
    let logNArr = 0;
    let nlogNArr = 0;

    while(n > 1) // O(log n)
    {
        n = Math.floor(n /2);

        for(let j = 0; j < i; j++) // O(n)
        {
            if(nArr < i)
            {
                nArr++;
            }

            nlogNArr++;
        }

        logNArr++;
    }

    console.log(nArr);
    console.log(logNArr);
    console.log(nlogNArr);
}

nLogN(8);