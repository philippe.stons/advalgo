/*
    La complexitée d'une fonction linéaire augmentera de manière linéaire par rapport à l'entrée.
*/

function linearFunction(arr) // O(n)
{
    for(let i = 0; i < arr.length; i++) // O(n)
    {
        console.log(arr.length);    // O(1)
        let test = ((2000* 5 ) / 2) + arr.length; // O(1)
        console.log(test); // O(1)
    }
}

function linearFunction2(arr) // O(n + n) => O(2n)
{
    for(let i = 0; i < arr.length; i++) // O(n)
    {
        console.log(arr.length);    // O(1)
        let test = ((2000* 5 ) / 2) + arr.length; // O(1)
        console.log(test); // O(1)
    }

    for(let i = 0; i < arr.length; i++) // O(n)
    {
        console.log(arr.length);    // O(1)
        let test = ((2000* 5 ) / 2) + arr.length; // O(1)
        console.log(test); // O(1)
    }
}