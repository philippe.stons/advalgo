/*
    à chaque ittération, on va sélectionner le plus petits élément
    de la partition non triée et le mettre dans une parition triée.

    On va garder en mémoire le minimum actuel (v) et l'élément actuel (^).
    
    On démarre avec v et ^ qui pointe au début de notre array.
     v
    [2, 8, 5, 3, 9, 4, 1]
     ^

    On parcoure ensuite notre array à la recherche d'un élément plus petit :
    Une fois trouvé on place v dessus et on continue de parcourir le tableau
    à la recherche d'un nouveau minimum.
     v                                          v
    [2, 8, 5, 3, 9, 4, 1] -> [2, 8, 5, 3, 9, 4, 1]
     ------------------^                        ^

    Comme on est à la fin de notre tableau, on inverse cette élément avec celui
    du début de notre tableau, qui devient notre premier élément trié.
    On déplace ensuite les pointeurs au premnier élément non trié.
        v
    [1| 8, 5, 3, 9, 4, 2]
        ^
    On déplace ensuite v et à la recherche du minimum, quand on trouve un 
    élément plus petit, on déplace ^ à cet élément.
        ---v                     ------v                  ---------------v
    [1| 8, 5, 3, 9, 4, 2] -> [1| 8, 5, 3, 9, 4, 2] -> [1| 8, 5, 3, 9, 4, 2]
        ---^                     ------^                  ---------------^

    Une fois arrivé à la fin on inverse le première élément non trié avec le 
    minimum.
           v
    [1, 2| 5, 3, 9, 4, 8]
           ^
    On répète enuiste l'opération :
           ---v                     ------------v
    [1, 2| 5, 3, 9, 4, 8] -> [1, 2| 5, 3, 9, 4, 8]
           ---^                     ---^
    
    On inverse donc cet élément et on le place dans notre parition triée.
              v
    [1, 2, 3| 5, 9, 4, 8]
              ^
              ------v                  ---------v
    [1, 2, 3| 5, 9, 4, 8] -> [1, 2, 3| 5, 9, 4, 8]
              ------^                  ------^
    
                 v
    [1, 2, 3, 4| 9, 5, 8]
                 ^
                 ---v                     ------v
    [1, 2, 3, 4| 9, 5, 8] -> [1, 2, 3, 4| 9, 5, 8]
                 ---^                     ---^

                    v
    [1, 2, 3, 4, 5| 9, 8]
                    ^
                    ---v 
    [1, 2, 3, 4, 5| 9, 8]
                    ---^
    
    Nous sommes enfin arrivé à la fin de notre tableau, celui-ci est donc trié.
                       v
    [1, 2, 3, 4, 5, 8| 9]
                       ^ 
*/
let arr = [2, 8, 5, 3, 9, 4, 1];
console.log(selectionSort(arr));

function selectionSort(arr)
{
    
    for(let i = 0; i < arr.length - 1; i++)
    {
        let minIndex = i;
        
        for(let j = i + 1; j < arr.length; j++)
        {
            if(arr[j] < arr[minIndex])
            {
                minIndex = j;
            }
        }
        
        if(i === minIndex)
        {
            break;
        }

        let tmp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = tmp;
    }

    return arr;
}