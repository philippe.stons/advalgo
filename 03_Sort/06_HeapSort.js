/*
    Quelques définitions :
        - un heap est un binary tree ordonné.
        - C'est un max heap quand le parent > que l'enfant.

    Exemple d'un max heap :
    [9, 8, 3, 1, 5, 2]

            9
           / \
          /   \
         8     3
        / \    /
        1  5  2

    Nous allons avoir besoin de plusieurs fonctions :
        - build max heap :
            Celle-ci créera un max heap à partir d'un unordered array.
        - heapify :
            Elle fera la même chose que le max heap, mais supposera qu'une
            partie est déjà ordonnée.

    Exemple de fonctionnement de l'algorithme :

    [2, 8, 5, 3, 9, 1]
    
            2
           / \
          /   \
         8     5
        / \    /
        3  9  1

    On va appeler max heap pour créer un max heap de l'array :

    [9, 8, 5, 3, 2, 1]
    
            9
           / \
          /   \
         8     5
        / \    /
        3  2  1

    Nous allons maintenant swap le tree root avec le dernière élément de l'array.

    [1, 8, 5, 3, 2, 9]
    
            1
           / \
          /   \
         8     5
        / \    /
        3  2  9

    Nous pouvons maintenant considéré 9 comme étant trié (vu que c'étais l'élément le plus grand).

    [1, 8, 5, 3, 2, 9]
    
            1
           / \
          /   \
         8     5
        / \    /
        3  2  9
    Nous pouvons maintenant appeler heapify car seulement notre 1 n'est plus à sa place dans le tree.
    9 a été retiré du tree.

    [1, 8, 5, 3, 2| 9]  ->  [8, 1, 5, 3, 2 | 9]  -> [8, 3, 5, 1, 2 | 9]
    
            1                   8               8
           / \                 / \             / \
          /   \               /   \           /   \
         8     5     -->     1     5   -->   3     5
        / \   //            / \             / \
        3  2  9            3  2            1  2

    Maintenant que nous avons denouveau un max heap nous pouvons interchanger le max avec le dernière 
    élément du tableau. Nous pouvons considérer 8 comme étant trié.

    [2, 3, 5, 1 | 8 | 9]
    
            2
           / \ 
          /   \ 
         3     5
        / \\
        1  8

    Nous allons rappeler heapify sur cette pour recréer le maxheap :

    [2, 3, 5, 1 | 8 | 9] -> [5, 3, 2, 1 | 8 | 9] 
    
            2               5
           / \             / \
          /   \           /   \
         3     5         3     2
        /               /
        1               1

    Nous allons encore une fois place le tree root à la fin de notre tableau :

    [1, 3, 2 | 5 | 8 | 9] 
    
            1
           / \ 
          /   \ 
         3     2 
        //
        5

    On rappel heapify :

    [1, 3, 2 | 5 | 8 | 9]  -> [3, 1, 2 | 5 | 8 | 9]
    
          1               3
         / \    -->      / \
        3   2           1   2

    Et on replace le root à la fin :

    [2, 1 | 3 | 5 | 8 | 9]
    
          2
         / \\
        1   3
        
    On rappel heapify (qui ne changera rien dans ce cas)

    [2, 1 | 3 | 5 | 8 | 9]
    
        2
       /
      1

    et ensuite on replace notre root à la fin : 

    [1 | 2 | 3 | 5 | 8 | 9]
    
        1
       //
      2

    Maintenant nous n'avons plus qu'un index le tableau est donc trié.
*/

console.log(buildMaxHeap([2, 8, 5, 3, 9, 1]));
console.log(heapSort([2, 8, 5, 3, 9, 1]));

function heapSort(arr)
{
    buildMaxHeap(arr);

    let end = arr.length - 1;

    while(end > 0)
    {
        swap(arr, 0, end);
        heapify(arr, 0, end);
        end--;
    }

    return arr;
}

function swap(arr, i, j)
{
    let tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
}

function buildMaxHeap(arr)
{
    let i = Math.floor((arr.length / 2) - 1);

    while(i >= 0)
    {
        heapify(arr, i, arr.length);
        i--;
    }

    return arr;
}

function heapify(arr, start, end)
{
    let i = start;
    while(i < end)
    {
        let index = i;
        let leftChildIndex = (2 * i) + 1;
        let rightChildIndex = leftChildIndex + 1;

        if(leftChildIndex < end && arr[leftChildIndex] > arr[index])
        {
            index = leftChildIndex;
        } 
        
        if(rightChildIndex < end && arr[rightChildIndex] > arr[index])
        {
            index = rightChildIndex;
        }

        if(index === i)
        {
            return;
        }

        swap(arr, i, index);
        console.log(i, index, arr);

        i = index;
    }
}