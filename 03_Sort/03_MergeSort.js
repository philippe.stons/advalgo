/*
    Pour résoudre notre problème de tri, nous allons le réduire en plusieurs plus petits problèmes.

    [2, 8, 5, 3, 9, 4, 1, 7]

    Dans le merge sort, nous allons divisé notre tableau en deux, jusqu'a ne plus qu'avoir des 
    items individuel.

       [2, 8, 5, 3 | 9, 4, 1, 7]
      [2, 8 | 5, 3 | 9, 4 | 1, 7]
    [2 | 8 | 5 | 3 | 9 | 4 | 1 | 7]

    Maintenant que nous avons fini de splitter l'array, nous pouvons commencer le tri.
    Nous allons pour ce faire, regrouper les items deux par deux et les comparer/trié.
    [2 | 8 | 5 | 3 | 9 | 4 | 1 | 7]
      [2, 8 | 3, 5 | 4, 9 | 1, 7]

    Nos array temporaire sont trié, nous pouvons donc passer à l'étape suivante :
    [2, 8 | 3, 5 | 4, 9 | 1, 7]
     [2, 3, 5, 8 | 1, 4, 7, 9]

    Nous avons enfin plus que deux, tableau trié, nous pouvons donc les merge ensemble en les triants :
     [2, 3, 5, 8 | 1, 4, 7, 9]
      [1, 2, 3, 4, 5, 7, 8, 9]

    Nous avons maintenant notre tableau trié.
*/

let arr = [2 , 8 , 5 , 3 , 9 , 4 , 1 , 7];
console.log(mergeSort2(arr, 0, arr.length));

function mergeSort(arr)
{
    if(arr.length < 2)
    {
        return arr;
    }

    let middle = Math.floor(arr.length / 2);
    let leftArr = arr.slice(0, middle);
    let rightArr = arr.slice(middle, arr.length);

    leftArr = mergeSort(leftArr);
    rightArr = mergeSort(rightArr);

    return merge(leftArr, rightArr);
}

function merge(leftArr, rightArr)
{
    let res = [];
    let leftIndex = 0;
    let rightIndex = 0

    // [2]
    // [1]
    // res [1]
    // left [1, 2, 3, 5]
    // right [4, 6, 7, 8]
    // res [1, 2, 3, 4, 5]

    while(leftIndex < leftArr.length && rightIndex < rightArr.length)
    {
        if(leftArr[leftIndex] < rightArr[rightIndex])
        {
            res.push(leftArr[leftIndex]);
            leftIndex++;
        } else 
        {
            res.push(rightArr[rightIndex]);
            rightIndex++;
        }
    }

    return res.concat(leftArr.slice(leftIndex)).concat(rightArr.slice(rightIndex));
}

function mergeSort2(arr, start, end)
{
    if(end - start < 2)
    {
        return arr;
    }

    const middle = Math.floor((start + end) / 2);
    mergeSort2(arr, start, middle);
    mergeSort2(arr, middle, end);

    return merge2(arr, start, middle, end);
}

function merge2(arr, start, middle, end)
{
    let res = [];
    let leftIndex = start;
    let rightIndex = middle;

    while(leftIndex < middle && rightIndex < end)
    {
        if(arr[leftIndex] < arr[rightIndex])
        {
            res.push(arr[leftIndex]);
            leftIndex++;
        } else 
        {
            res.push(arr[rightIndex]);
            rightIndex++;
        }
    }

    if(rightIndex < end)
    {
        for(let i = rightIndex; i < end; i++)
        {
            res.push(arr[i]);
        }
    } else if(leftIndex < middle)
    {
        for(let i = leftIndex; i < middle; i++)
        {
            res.push(arr[i]);
        }
    }

    let ri = 0;
    for(let i = start; i < end; i++)
    {
        arr[i] = res[ri];
        ri++;
    }

    return arr;
}