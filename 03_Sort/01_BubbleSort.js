/*
    [5, 1, 3, 2, 9, 8]
    
    Le bubble sort est l'algorithme la plus simple :
        - On va parcourir le tableau et comparer chaque élément avec
          avec l'élément suivant.
        - Si l'élément du bas est plus grand que celui de l'index supérieur
          alors on les inverse.
        - On arrête de parcourir une fois qu'on n'a plus fait aucun swap d'élément.

    1ere iteration :
    [5 <=> 1, 3, 2, 9, 8]
    [1, 5 <=> 3, 2, 9, 8]
    [1, 3, 5 <=> 2, 9, 8]
    [1, 3, 2, 5 <=> 9, 8]
    [1, 3, 2, 5, 9 <=> 8]
    [1, 3, 2, 5, 8, 9]

    2eme iteration :
    [1 <=> 3, 2, 5, 8, 9]
    [1, 3 <=> 2, 5, 8, 9]
    [1, 2, 3 <=> 5, 8, 9]
    [1, 2, 3, 5 <=> 8, 9]
    [1, 2, 3, 5, 8 <=> 9]
    [1, 2, 3, 5, 8, 9]

    3eme iteration :
    [1 <=> 2, 3, 5, 8, 9]
    [1, 2 <=> 3, 5, 8, 9]
    [1, 2, 3 <=> 5, 8, 9]
    [1, 2, 3, 5 <=> 8, 9]
    [1, 2, 3, 5, 8 <=> 9]
    [1, 2, 3, 5, 8, 9]

    Comme on peut le constater, après la première ittération le dernière élément du tableau est déjà
    le plus grand.

    On peut donc optimiser l'algorithme en ne regardant plus les éléments que l'on sait trié 
    (soit le dernier index de chaque ittération).
*/

arr = [5, 1, 3, 2, 9, 8];
console.log(arr);
console.log(bubbleSort(arr));

function bubbleSort(arr)
{
    let itterration = 0;

    for(let i = 0; i < arr.length; i++)
    {
        console.log(`iterration : ${i + 1}`);
        let swapped = false;
        for(let j = 0; j < arr.length - i - 1; j++)
        {
            itterration++;
            if(arr[j] > arr[j + 1])
            {
                swapped = true;
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                console.log(arr);
            }
        }

        if(!swapped)
        {
            console.log(`Fini en : ${itterration}`);
            break;
        }
    }

    return arr;
}