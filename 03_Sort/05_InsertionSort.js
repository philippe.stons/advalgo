/*
    Fonctionnement :
        - On va parcourir le tableau de droite à gauche.
        - On compare chque élément avec l'élément à sa gauche.
        - Inserer l'item à sa position dans le tableau.
        - Nous obtiendront des paritions triées et non triées.

    On parcoure le tableau, et on compare l'index actuel avec l'élément à sa gauche.
    N'ayant pas d'élément à sa gauche, nous le marquons comme trié et on incrémente v.
     v                          v
    [2, 8, 5, 3, 9, 4] -> [|2|, 8, 5, 3, 9, 4]

    Maintenant nous comparons 2 et 8, 2 < 8 nous ajoutons 8 à la partition triée.
    Et nous incrémentons v.
          v                         v
    [|2|, 8, 5, 3, 9, 4] -> [|2, 8| 5, 3, 9, 4]

    Maintenant nous comparons 8 et 5, 8 > 5, nous les inversons 8 par 5 dans
    nos 3 premier item sont donc dansla parition triée, on incrémente ensuite v.
            v                         v
    [|2, 8| 5, 3, 9, 4] -> [|2, 5, 8| 3, 9, 4]

    8 > 3, nous inversons donc leur position, puis 5 > 3, nous les inversons aussi.
               v                      v                      v
    [|2, 5, 8| 3, 9, 4] -> [|2, 5, 3| 8, 9, 4] -> [|2, 3, 5, 8| 9, 4] 
                  v                         v
    [|2, 3, 5, 8| 9, 4] -> [|2, 3, 5, 8, 9| 4]
                     v                      v
    [|2, 3, 5, 8| 9, 4] -> [|2, 3, 5, 8, 4| 9] -> [|2, 3, 5, 4, 8| 9] -> [|2, 3, 4, 5, 8, 9|]
*/
let arr = [2, 8, 5, 3, 9, 4];
console.log(insertionSort(arr));

function insertionSort(arr)
{
    for(let i = 1; i < arr.length; i++)
    {
        let key = arr[i];
        let j = i - 1;

        while(j >= 0 && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }

        arr[j + 1] = key;
    }

    return arr;
}