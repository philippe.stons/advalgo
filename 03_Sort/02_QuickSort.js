/*
    Le quick sort, comme le merge sort est une algorithme qui va diviser notre tableau 
    pour le trier. Pour ce faire on va utiliser un pivot et trier ce qui est plus grand
    à sa droite et ce qui est plus petit à sa gauche.

    On va d'abord devoir passer une première fois en choissisant un pivot (généralement on
    utilisera l'index le plus bas). Ensuite on trie tout ce qui est plus grand que cet index.
    à sa droite et le reste à sa gauche.

    Une fois qu'on a classer ces éléments on répète l'opération sur les deux sous tableau,
    donc sur les éléments à sa droite, et sur les éléments à sa gauche.

    On fait ensuite celà de façon récursive jusqu'à ne plus qu'avoir un tableau d'une taille de 1.
    
    [2, 6, 5, 1, 7, 3]

    1) on sélectionne un pivot.
                    p
    [2, 6, 5, 1, 7, 3]
    
    (^) => l'index de la partition
    (v) => le pointeur pour parcourir le tableau.
     v              p
    [2, 6, 5, 1, 7, 3]
     ^
    2 > 3

    On inverse v et ^ et on incrémente v et ^
     v              p
    [2, 6, 5, 1, 7, 3]
     ^
    6 > 3 ? oui donc incrément v.
        v           p
    [2, 6, 5, 1, 7, 3]
        ^
    
    5 > 3 ? oui donc incrément v.
           v        p
    [2, 6, 5, 1, 7, 3]
        ^
    1 > 3 ? non donc on swap v et ^ et on les incrémente.
              v     p               v     p                  v  p
    [2, 6, 5, 1, 7, 3] -> [2, 1, 5, 6, 7, 3] -> [2, 1, 5, 6, 7, 3]
        ^                     ^                        ^
    7 > 3 ? Oui donc on incrémente v, sauf ici on est à la fin du tableau.
    Donc on va inverser p et ^
                 v  p                  v  p
    [2, 1, 5, 6, 7, 3] -> [2, 1, 3, 6, 7, 5]
           ^                     ^
    2 > 1 ? oui, on incémente v. v = p donc on inverse p et ^
    On fait, deux parition, comme elles n'ont qu'une élément elles sont donc triée.
     v  p      v  p
    [2, 1] -> [1, 2]
     ^         ^
    
    6 > 5 ? Oui donc on incrémente v
     v     p
    [6, 7, 5]
     ^

    7 > 5 ? Oui donc on incrémente v. v = p donc on inverse ^ et p.
        v  p         v  p
    [6, 7, 5] -> [5, 7, 6]
     ^            ^
    7 > 6 ? oui donc incrémentation de v. v = p donc switch ^ et p.
     v  p      v  p
    [7, 6] -> [6, 7]
     ^         ^

    [1, 2] [3] [5] [6, 7] => [1, 2, 3, 5, 6, 7]
*/

function partition(arr, low, high)
{
    let pivot = arr[high];

    let part = low;

    for(let i = low; i < high; i++)
    {
        if(arr[i] < pivot)
        {
            let tmp = arr[i];
            arr[i] = arr[part];
            arr[part] = tmp;
            part++;
        }
    }

    arr[high] = arr[part];
    arr[part] = pivot;

    return part;
}

function quickSort(arr, low, high)
{
    if(low < high)
    {
        let part = partition(arr, low, high);
        console.log(low, high, part);

        quickSort(arr, low, part - 1);
        quickSort(arr, part + 1, high);
    }

    return arr;
}

let arr = [2, 6, 5, 1, 7, 3];
console.log(quickSort(arr, 0, arr.length - 1));